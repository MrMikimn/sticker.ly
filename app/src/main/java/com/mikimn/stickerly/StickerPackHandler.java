/*
 * Copyright (c) WhatsApp Inc. and its affiliates.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree.
 */

package com.mikimn.stickerly;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.JsonWriter;
import android.util.Log;

import com.mikimn.stickerly.model.Sticker;
import com.mikimn.stickerly.model.StickerPack;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import androidx.annotation.NonNull;

import static com.mikimn.stickerly.StickerContentProvider.ANDROID_APP_DOWNLOAD_LINK_IN_QUERY;
import static com.mikimn.stickerly.StickerContentProvider.IOS_APP_DOWNLOAD_LINK_IN_QUERY;
import static com.mikimn.stickerly.StickerContentProvider.LICENSE_AGREENMENT_WEBSITE;
import static com.mikimn.stickerly.StickerContentProvider.PRIVACY_POLICY_WEBSITE;
import static com.mikimn.stickerly.StickerContentProvider.PUBLISHER_EMAIL;
import static com.mikimn.stickerly.StickerContentProvider.PUBLISHER_WEBSITE;
import static com.mikimn.stickerly.StickerContentProvider.STICKER_FILE_EMOJI_IN_QUERY;
import static com.mikimn.stickerly.StickerContentProvider.STICKER_FILE_NAME_IN_QUERY;
import static com.mikimn.stickerly.StickerContentProvider.STICKER_PACK_ICON_IN_QUERY;
import static com.mikimn.stickerly.StickerContentProvider.STICKER_PACK_IDENTIFIER_IN_QUERY;
import static com.mikimn.stickerly.StickerContentProvider.STICKER_PACK_NAME_IN_QUERY;
import static com.mikimn.stickerly.StickerContentProvider.STICKER_PACK_PUBLISHER_IN_QUERY;

public class StickerPackHandler {

    private static final String TAG = "StickerPackHandler";

    /**
     * Get the list of sticker packs for the sticker content provider
     */
    @NonNull
    public static ArrayList<StickerPack> fetchStickerPacks(Context context) throws IllegalStateException {
        final Cursor cursor = context.getContentResolver().query(StickerContentProvider.AUTHORITY_URI, null, null, null, null);
        if (cursor == null) {
            throw new IllegalStateException("could not fetch from content provider, " + BuildConfig.CONTENT_PROVIDER_AUTHORITY);
        }
        HashSet<String> identifierSet = new HashSet<>();
        final ArrayList<StickerPack> stickerPackList = fetchFromContentProvider(cursor);
        for (StickerPack stickerPack : stickerPackList) {
            if (identifierSet.contains(stickerPack.identifier)) {
                throw new IllegalStateException("sticker pack identifiers should be unique, there are more than one pack with identifier:" + stickerPack.identifier);
            } else {
                identifierSet.add(stickerPack.identifier);
            }
        }
//        if (stickerPackList.isEmpty()) {
//            throw new IllegalStateException("There should be at least one sticker pack in the app");
//        }
        for (StickerPack stickerPack : stickerPackList) {
            final List<Sticker> stickers = getStickersForPack(context, stickerPack);
            stickerPack.setStickers(stickers);
            StickerPackValidator.verifyStickerPackValidity(context, stickerPack);
        }
        return stickerPackList;
    }

    @NonNull
    private static List<Sticker> getStickersForPack(Context context, StickerPack stickerPack) {
        final List<Sticker> stickers = fetchFromContentProviderForStickers(context, stickerPack.identifier, context.getContentResolver());
        for (Sticker sticker : stickers) {
            final byte[] bytes;
            try {
                bytes = fetchStickerAsset(stickerPack.identifier, sticker.imageFileName, context.getContentResolver());
                if (bytes.length <= 0) {
                    throw new IllegalStateException("Asset file is empty, pack: " + stickerPack.name + ", sticker: " + sticker.imageFileName);
                }
                sticker.setSize(bytes.length);
            } catch (IOException | IllegalArgumentException e) {
                throw new IllegalStateException("Asset file doesn't exist. pack: " + stickerPack.name + ", sticker: " + sticker.imageFileName, e);
            }
        }
        return stickers;
    }


    @NonNull
    private static ArrayList<StickerPack> fetchFromContentProvider(Cursor cursor) {
        ArrayList<StickerPack> stickerPackList = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                final String identifier = cursor.getString(cursor.getColumnIndexOrThrow(STICKER_PACK_IDENTIFIER_IN_QUERY));
                final String name = cursor.getString(cursor.getColumnIndexOrThrow(STICKER_PACK_NAME_IN_QUERY));
                final String publisher = cursor.getString(cursor.getColumnIndexOrThrow(STICKER_PACK_PUBLISHER_IN_QUERY));
                final String trayImage = cursor.getString(cursor.getColumnIndexOrThrow(STICKER_PACK_ICON_IN_QUERY));
                final String androidPlayStoreLink = cursor.getString(cursor.getColumnIndexOrThrow(ANDROID_APP_DOWNLOAD_LINK_IN_QUERY));
                final String iosAppLink = cursor.getString(cursor.getColumnIndexOrThrow(IOS_APP_DOWNLOAD_LINK_IN_QUERY));
                final String publisherEmail = cursor.getString(cursor.getColumnIndexOrThrow(PUBLISHER_EMAIL));
                final String publisherWebsite = cursor.getString(cursor.getColumnIndexOrThrow(PUBLISHER_WEBSITE));
                final String privacyPolicyWebsite = cursor.getString(cursor.getColumnIndexOrThrow(PRIVACY_POLICY_WEBSITE));
                final String licenseAgreementWebsite = cursor.getString(cursor.getColumnIndexOrThrow(LICENSE_AGREENMENT_WEBSITE));
                final StickerPack stickerPack = new StickerPack(identifier, name, publisher, trayImage, publisherEmail, publisherWebsite, privacyPolicyWebsite, licenseAgreementWebsite);
                stickerPack.setAndroidPlayStoreLink(androidPlayStoreLink);
                stickerPack.setIosAppStoreLink(iosAppLink);
                stickerPackList.add(stickerPack);
            } while (cursor.moveToNext());
        }
        return stickerPackList;
    }

    @NonNull
    private static List<Sticker> fetchFromContentProviderForStickers(Context context, String identifier, ContentResolver contentResolver) {
        Uri uri = getStickerListUri(identifier);

        final String[] projection = {STICKER_FILE_NAME_IN_QUERY, STICKER_FILE_EMOJI_IN_QUERY};
        final Cursor cursor = contentResolver.query(uri, projection, null, null, null);
        List<Sticker> stickers = new ArrayList<>();
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                final String name = cursor.getString(cursor.getColumnIndexOrThrow(STICKER_FILE_NAME_IN_QUERY));
                final String emojisConcatenated = cursor.getString(cursor.getColumnIndexOrThrow(STICKER_FILE_EMOJI_IN_QUERY));
                stickers.add(new Sticker(name, Arrays.asList(emojisConcatenated.split(","))));
            } while (cursor.moveToNext());
        }
        if (cursor != null) {
            cursor.close();
        }
        return stickers;
    }

    public static byte[] fetchStickerAsset(@NonNull final String identifier, @NonNull final String name, ContentResolver contentResolver) throws IOException {
        Log.i("Handler", "Fetching asset " + identifier + "/" + name);
        try (final InputStream inputStream = contentResolver.openInputStream(getStickerUri(identifier, name));
             final ByteArrayOutputStream buffer = new ByteArrayOutputStream()) {
            if (inputStream == null) {
                throw new IOException("cannot read sticker asset:" + identifier + "/" + name);
            }
            int read;
            byte[] data = new byte[16384];

            while ((read = inputStream.read(data, 0, data.length)) != -1) {
                buffer.write(data, 0, read);
            }
            return buffer.toByteArray();
        }
    }

    private static Uri getStickerListUri(String identifier) {
        return new Uri.Builder().scheme(StickerContentProvider.CONTENT_SCHEME).authority(BuildConfig.CONTENT_PROVIDER_AUTHORITY).appendPath(StickerContentProvider.STICKERS).appendPath(identifier).build();
    }

    public static Uri getStickerUri(String identifier, String stickerName) {
        return new Uri.Builder().scheme(StickerContentProvider.CONTENT_SCHEME).authority(BuildConfig.CONTENT_PROVIDER_AUTHORITY).appendPath(StickerContentProvider.STICKERS_ASSET).appendPath(identifier).appendPath(stickerName).build();
    }

    private static synchronized boolean updateStickerPacks(@NonNull Context context, @NonNull List<StickerPack> stickerPacks) {
        try {
            File contentFile = new File(context.getFilesDir().getAbsolutePath(), StickerContentProvider.CONTENT_FILE_NAME);

            if(contentFile.exists() || (!contentFile.exists() && contentFile.createNewFile())) {
                JsonWriter writer = new JsonWriter(new FileWriter(contentFile));
                writer.setIndent("  ");
                ContentFileParser.writeStickerPacks(stickerPacks, writer);
                writer.flush();
                writer.close();
                return true;
            } else {
                Log.e(TAG, "Error writing sticker packs: can't create file " + StickerContentProvider.CONTENT_FILE_NAME);
                return false;
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public synchronized static boolean writeStickerPack(Context context, StickerPack pack) {
        List<StickerPack> stickerPacks = fetchStickerPacks(context);
        if(stickerPacks.contains(pack)) {
            // In case of edit
            stickerPacks.set(stickerPacks.indexOf(pack), pack);
        } else {
            stickerPacks.add(pack);
        }

        return updateStickerPacks(context, stickerPacks);
    }

    public synchronized static StickerPack deleteStickerPack(Context context, String identifier) {
        List<StickerPack> stickerPacks = fetchStickerPacks(context);

        StickerPack deletedPack = null;
        for(StickerPack pack : stickerPacks) {
            if(pack.identifier.equals(identifier)) {
                deletedPack = pack;
                break;
            }
        }

        if(deletedPack != null) {
            stickerPacks.remove(deletedPack);
        }

        updateStickerPacks(context, stickerPacks);

        File packDirectory = new File(context.getFilesDir().getAbsolutePath(), identifier);
        if(packDirectory.exists() && packDirectory.isDirectory()) {
            String[] children = packDirectory.list();
            for (String child : children)
            {
                new File(packDirectory, child).delete();
            }
            packDirectory.delete();
        }
        return deletedPack;
    }
}

package com.mikimn.stickerly.newpack;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.mikimn.stickerly.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class NewStickerPackAdapter extends RecyclerView.Adapter<NewStickerPackViewHolder> {
    private List<Uri> dataset;
    private ItemDeletedListener listener;

    NewStickerPackAdapter(@NonNull List<Uri> dataset) {
        this.dataset = dataset;
    }

    void addImage(Uri uri) {
        dataset.add(uri);
    }

    List<Uri> getImages() {
        return dataset;
    }

    void setItemDeleteListener(ItemDeletedListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public NewStickerPackViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.new_sticker_pack_list_item, viewGroup, false);
        return new NewStickerPackViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NewStickerPackViewHolder newStickerPackViewHolder, int i) {
        Uri imageUri = dataset.get(i);
        newStickerPackViewHolder.setImage(imageUri);
        ImageView deleteImageButton = newStickerPackViewHolder.deleteImageButton;
        deleteImageButton.setClickable(true);
        deleteImageButton.setOnClickListener(v -> {
            int index = dataset.indexOf(imageUri);
            dataset.remove(index);
            notifyItemRemoved(index);
            if(listener != null) {
                listener.onItemDeleted(index);
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }

    interface ItemDeletedListener {
        void onItemDeleted(int index);
    }
}

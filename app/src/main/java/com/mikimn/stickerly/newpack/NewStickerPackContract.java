package com.mikimn.stickerly.newpack;

import android.content.ContentValues;
import android.net.Uri;

import java.util.List;

public class NewStickerPackContract {

    interface View {
        // void onImageProcessed(Uri imageUri, String imageName);
        void onImagesProcessed(List<Uri> imageUris);
        // void onTrayImageProcessed(Uri imageUri, String imageName);
        void onStickerPackSaved(Uri resUri);
        void onError(Throwable t);
        void showLoadingBar();
        void hideLoadingBar();
    }

    interface Presenter {
        // TODO Delete
//        void processImage(int stickerCount, String stickerPackIdentifier, Uri imageUri);
//        void processTrayImage(String stickerPackIdentifier, Uri imageUri);
        void saveStickerPack(List<Uri> imageUris, ContentValues values);
    }

}

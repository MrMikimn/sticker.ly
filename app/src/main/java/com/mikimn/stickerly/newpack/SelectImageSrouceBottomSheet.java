package com.mikimn.stickerly.newpack;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.button.MaterialButton;
import com.mikimn.stickerly.R;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class SelectImageSrouceBottomSheet extends BottomSheetDialogFragment implements View.OnClickListener {

    private OnImageSourceSelected listener;

    private MaterialButton cameraButton;
    private MaterialButton galleryButton;
    private MaterialButton drawButton;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.bottom_sheet_new_sticker_pack_image_source, container, false);

        cameraButton = view.findViewById(R.id.select_sticker_image_camera_button);
        galleryButton = view.findViewById(R.id.select_sticker_image_gallery_button);
        drawButton = view.findViewById(R.id.select_sticker_image_draw_button);

        if(view.getContext().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY)) {
            cameraButton.setOnClickListener(this);
            cameraButton.setVisibility(View.VISIBLE);
        } else {
            cameraButton.setVisibility(View.GONE);
        }

        galleryButton.setOnClickListener(this);
        drawButton.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {
        if(listener == null) return;

        switch (v.getId()) {
            case R.id.select_sticker_image_camera_button:
                listener.onSelected(ImageSource.Camera);
                break;
            case R.id.select_sticker_image_gallery_button:
                listener.onSelected(ImageSource.Gallery);
                break;
            case R.id.select_sticker_image_draw_button:
                listener.onSelected(ImageSource.Draw);
                break;
        }
    }

    public void setOnImageSourceSelectedListener(@NonNull OnImageSourceSelected listener) {
        this.listener = listener;
    }

    public enum ImageSource {
        Gallery,
        Camera,
        Draw
    }

    interface OnImageSourceSelected {
        void onSelected(ImageSource source);
    }
}

package com.mikimn.stickerly.newpack;

import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.mikimn.stickerly.R;

public class NewStickerPackViewHolder extends RecyclerView.ViewHolder {

    ImageView imageView;
    ImageView deleteImageButton;

    public NewStickerPackViewHolder(@NonNull View itemView) {
        super(itemView);
        imageView = itemView.findViewById(R.id.new_sticker_pack_list_item_image_view);
        deleteImageButton = itemView.findViewById(R.id.new_sticker_pack_item_delete_image);
    }

    public void setImage(Uri uri) {
        imageView.setImageURI(uri);
    }
}

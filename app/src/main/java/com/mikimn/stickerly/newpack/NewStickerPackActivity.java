package com.mikimn.stickerly.newpack;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.mikimn.stickerly.BaseActivity;
import com.mikimn.stickerly.R;
import com.mikimn.stickerly.StickerContentProvider;
import com.mikimn.stickerly.entry.EntryActivity;
import com.mikimn.stickerly.imageeditor.EditPhotoActivity;
import com.mikimn.stickerly.model.Sticker;
import com.mikimn.stickerly.model.StickerPack;
import com.mikimn.stickerly.utils.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NewStickerPackActivity extends BaseActivity implements
        View.OnClickListener,
        NewStickerPackContract.View,
        NewStickerPackAdapter.ItemDeletedListener,
        SelectImageSrouceBottomSheet.OnImageSourceSelected {

    public static final String INTENT_EXTRA_PACK_NAME = "extraStickerPackName";
    public static final String INTENT_EXTRA_PUBLISHER_NAME = "extraStickerPublisherName";
    public static final String INTENT_EXTRA_IS_EDIT = "extraIsEdit";
    public static final String INTENT_EXTRA_STICKER_PACK_DATA = "extraStickerPackData";

    final int REQUEST_CODE_IMAGE_PICKER = 100;
    public static final int RESULT_ERROR = 101;
    public static final int REQUEST_CODE_EDIT = 102;
    public static final int REQUEST_CODE_NEW_PACK = 103;
    private static final int REQUEST_IMAGE_CAPTURE = 104;

    private NewStickerPackPresenter presenter;

    @BindView(R.id.new_sticker_pack_add_fab) FloatingActionButton addStickerFAB;
    @BindView(R.id.add_sticker_pack_list) RecyclerView recyclerView;
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.progress_bar) ProgressBar progressBar;
    @BindView(R.id.empty_state_view) RelativeLayout emptyStateView;

    NewStickerPackAdapter adapter;
    // StickerPack stickerPack;
    private ContentValues contentValues;

    private SelectImageSrouceBottomSheet selectImageSrouceBottomSheet;

    private String mCameraPhotoImagePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_sticker_pack);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        presenter = new NewStickerPackPresenter(this);
        selectImageSrouceBottomSheet = new SelectImageSrouceBottomSheet();
        selectImageSrouceBottomSheet.setOnImageSourceSelectedListener(this);

        Intent intent = getIntent();

        if(intent.hasExtra(INTENT_EXTRA_IS_EDIT) && intent.getBooleanExtra(INTENT_EXTRA_IS_EDIT, false)) {
            if(!intent.hasExtra(INTENT_EXTRA_STICKER_PACK_DATA)) {
                setResult(RESULT_ERROR);
                finish();
            } else {
                // Existing pack
                StickerPack stickerPack = intent.getParcelableExtra(INTENT_EXTRA_STICKER_PACK_DATA);

                contentValues = getContentValues(stickerPack.name, stickerPack.publisher, stickerPack.identifier);
                contentValues.put(StickerContentProvider.STICKER_PACK_ICON_IN_QUERY, stickerPack.trayImageFile);

                List<Uri> imageUris = new ArrayList<>();
                for (Sticker s : stickerPack.getStickers()) {
                    File imageFile = new File(getFilesDir().getAbsolutePath(), stickerPack.identifier + "/" + s.imageFileName);
                    Uri uri = Uri.fromFile(imageFile);
                    imageUris.add(uri);
                }

                adapter = new NewStickerPackAdapter(imageUris);
                setTitle("Edit Pack");
                getSupportActionBar().setSubtitle(stickerPack.name);
            }
        } else {
            // New pack
            String packName = intent.getStringExtra(INTENT_EXTRA_PACK_NAME);
            String publisherName = intent.getStringExtra(INTENT_EXTRA_PUBLISHER_NAME);
            String stickerPackIdentifier = FileUtils.md5(packName + (new Date()).toString());

            contentValues = getContentValues(packName, publisherName, stickerPackIdentifier);

            getSupportActionBar().setSubtitle(packName);
            adapter = new NewStickerPackAdapter(new ArrayList<>());
        }

        addStickerFAB.setOnClickListener(this);
        adapter.setItemDeleteListener(this);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 5));
        recyclerView.setAdapter(adapter);

        updateEmptyState();
    }

    private ContentValues getContentValues(String stickerPackName, String publisherName, String stickerPackIdentifier) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(StickerContentProvider.STICKER_PACK_NAME_IN_QUERY, stickerPackName);
        contentValues.put(StickerContentProvider.STICKER_PACK_PUBLISHER_IN_QUERY, publisherName);
        contentValues.put(StickerContentProvider.STICKER_PACK_IDENTIFIER_IN_QUERY, stickerPackIdentifier);
        return contentValues;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.new_sticker_pack_add_fab:
                selectImageSrouceBottomSheet.showNow(getSupportFragmentManager(), SelectImageSrouceBottomSheet.class.getName());
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.new_sticker_pack_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish(); // TODO Change to finishActivity(resCode)
                return true;
            case R.id.new_sticker_pack_done:
                presenter.saveStickerPack(adapter.getImages(), contentValues);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private String getPathFromURI(Uri contentUri) {
        String res = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(contentUri, proj, "", null, "");
        if (cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            res = cursor.getString(column_index);
        }
        cursor.close();
        return res;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_IMAGE_PICKER && resultCode == RESULT_OK) {

            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            if(selectedImage != null) {
                // Get the cursor
                Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                // Move to first row
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String imgDecodableString = cursor.getString(columnIndex);
                cursor.close();

                startActivityForResult(new Intent(this, EditPhotoActivity.class)
                        .putExtra(EditPhotoActivity.INTENT_EXTRA_IMAGE_NAME, getPathFromURI(selectedImage)), EditPhotoActivity.REQUEST_EDIT_PHOTO);
            }
        } else if (requestCode == EditPhotoActivity.REQUEST_EDIT_PHOTO && resultCode == RESULT_OK) {

            Uri imageUri = data.getParcelableExtra(EditPhotoActivity.INTENT_EXTRA_IMAGE_URI);
            handleNewImageInserted(imageUri);

        } else if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {

            String imagePath = mCameraPhotoImagePath;
            startActivityForResult(new Intent(NewStickerPackActivity.this, EditPhotoActivity.class)
                    .putExtra(EditPhotoActivity.INTENT_EXTRA_IMAGE_NAME, imagePath), EditPhotoActivity.REQUEST_EDIT_PHOTO);
        }
    }

    private void handleNewImageInserted(Uri imageUri) {
        adapter.addImage(imageUri);
        adapter.notifyItemInserted(adapter.getItemCount());
        updateEmptyState();
    }

    private void updateEmptyState() {
        if(adapter.getItemCount() == 0) {
            emptyStateView.setVisibility(View.VISIBLE);
        } else {
            emptyStateView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onSelected(SelectImageSrouceBottomSheet.ImageSource source) {
        switch (source) {
            case Gallery:
                startImagePicker();
                break;
            case Camera:
                startCamera();
                break;
            case Draw:
                startEmptyDrawing();
                break;
        }
        selectImageSrouceBottomSheet.dismiss();
    }

    private void startImagePicker() {
        FileUtils.requestReadWriteAccess(this, (boolean granted, Throwable t) -> {
            if(granted) {
                Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                // Start the Intent
                startActivityForResult(galleryIntent, REQUEST_CODE_IMAGE_PICKER);
                // creator.forResult(REQUEST_CODE_IMAGE_PICKER);
            } else {
                t.printStackTrace(); // TODO Handle
            }
        });
    }

    private void startCamera() {
        FileUtils.requestReadWriteAccess(NewStickerPackActivity.this, (boolean granted, Throwable t) -> {
            if(granted) {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                // Ensure that there's a camera activity to handle the intent
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    // Create the File where the photo should go
                    File photoFile = null;
                    try {
                        photoFile = FileUtils.createCameraImageFile(this);
                        mCameraPhotoImagePath = photoFile.getAbsolutePath();
                    } catch (IOException ex) {
                        Snackbar.make(recyclerView, "There was an error with image capturing. Make sure you have enough storage to save the image", Snackbar.LENGTH_LONG).show();
                    }
                    // Continue only if the File was successfully created
                    if (photoFile != null) {
                        Uri photoURI = FileProvider.getUriForFile(this,
                                getString(R.string.file_provider_authority),
                                photoFile);
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                        startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                    } else {
                        Snackbar.make(recyclerView, "There was an error with image capturing. Make sure you have enough storage to save the image", Snackbar.LENGTH_LONG).show();
                    }
                }
            } else {
                t.printStackTrace(); // TODO Handle
            }
        });
    }

    private void startEmptyDrawing() {
        Intent intent = new Intent(this, EditPhotoActivity.class);
        startActivityForResult(intent, EditPhotoActivity.REQUEST_EDIT_PHOTO);
    }

    @Override
    public void onImagesProcessed(List<Uri> imageUris) {

    }


    @Override
    public void onStickerPackSaved(Uri resUri) {
        setResult(RESULT_OK);
        startActivity(new Intent(this, EntryActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    @Override
    public void onError(Throwable t) {
        // TODO Remove on production
        t.printStackTrace();
        Toast.makeText(this, "Error: " + t.getMessage(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void showLoadingBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoadingBar() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onItemDeleted(int index) {
        updateEmptyState();
    }
}

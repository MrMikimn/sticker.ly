package com.mikimn.stickerly.newpack;

import android.content.ContentValues;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;

import com.bumptech.glide.Glide;
import com.mikimn.stickerly.StickerContentProvider;
import com.mikimn.stickerly.StickerPackValidator;
import com.mikimn.stickerly.utils.ImageUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import androidx.annotation.NonNull;

public class NewStickerPackPresenter implements NewStickerPackContract.Presenter {

    private static final String TRAY_ICON_FILE_NAME = "tray.png";

    private NewStickerPackActivity view;

    public NewStickerPackPresenter(NewStickerPackActivity activity) {
        this.view = activity;
    }

    private void saveStickerPackTrayImageFile(String stickerPackIdentifier, Uri imageUri, @NonNull OnTrayImageSavedListener listener) throws IOException, ExecutionException, InterruptedException {
        SaveStickerPackTrayImageTask task = new SaveStickerPackTrayImageTask(view, imageUri, stickerPackIdentifier, listener);
        task.execute();
    }

    private void saveStickerPackImageFiles(List<Uri> imageUris, String stickerPackIdentifier, OnImagesSavedListener listener) {
        SaveStickerPackImagesTask task = new SaveStickerPackImagesTask(view, imageUris, stickerPackIdentifier, listener);
        task.execute();
    }

    @Override
    public void saveStickerPack(List<Uri> imageUris, ContentValues values) {

        view.showLoadingBar();
        try {
            StickerPackValidator.validateStickerPack(imageUris);
        } catch (IllegalStateException ex) {
            view.hideLoadingBar();
            view.onError(ex);
            return;
        }

        String stickerPackIdentifier = values.getAsString(StickerContentProvider.STICKER_PACK_IDENTIFIER_IN_QUERY);
        File packDir = new File(view.getFilesDir().getAbsolutePath(), stickerPackIdentifier);
        if(packDir.exists() || (!packDir.exists() && packDir.mkdir())) {
            try {
                saveStickerPackTrayImageFile(stickerPackIdentifier, imageUris.get(0), new OnTrayImageSavedListener() {
                    @Override
                    public void onSaved(String trayImageName, Uri imagesUri) {
                        values.put(StickerContentProvider.STICKER_PACK_ICON_IN_QUERY, trayImageName);
                        saveStickerPackImageFiles(imageUris, stickerPackIdentifier, new OnImagesSavedListener() {
                            @Override
                            public void onSaved(List<String> stickerNamesList, List<Uri> newUris) {
                                view.runOnUiThread(() -> {
                                    String stickerNames = TextUtils.join(",", stickerNamesList);
                                    values.put(StickerContentProvider.STICKER_PACK_IMAGE_NAMES_IN_QUERY, stickerNames);
                                    Uri resUri = view.getContentResolver().insert(StickerContentProvider.INSERT_STICKER_PACK_URI, values);
                                    view.hideLoadingBar();
                                    view.onImagesProcessed(newUris);
                                    view.onStickerPackSaved(resUri);
                                });
                            }

                            @Override
                            public void onError(Throwable exception) {
                                view.runOnUiThread(() -> {
                                    view.hideLoadingBar();
                                    view.onError(exception);
                                });
                            }
                        });
                    }

                    @Override
                    public void onError(Throwable exception) {
                        view.runOnUiThread(() -> {
                            view.hideLoadingBar();
                            view.onError(exception);
                        });
                    }
                });
            } catch (IOException | IllegalStateException | ExecutionException | InterruptedException ex) {
                view.hideLoadingBar();
                view.onError(ex);
            }
        } else {
            // There was an error creating the files
            view.hideLoadingBar();
            view.onError(new RuntimeException("There was an error creating the sticker pack directory"));
        }
    }

    interface OnTrayImageSavedListener {
        void onSaved(String fileName, Uri imagesUri);
        void onError(Throwable exception);
    }

    interface OnImagesSavedListener {
        void onSaved(List<String> fileNames, List<Uri> imagesUris);
        void onError(Throwable exception);
    }

    static class SaveStickerPackTrayImageTask extends AsyncTask<Void, Void, Pair<String, Uri>> {
        private WeakReference<Context> contextWeakReference;

        @NonNull private OnTrayImageSavedListener listener;
        private String stickerPackIdentifier;
        private Uri imageUri;
        private Throwable exception;

        public SaveStickerPackTrayImageTask(@NonNull Context context, Uri imageUri, String stickerPackIdentifier, @NonNull OnTrayImageSavedListener listener) {
            this.contextWeakReference = new WeakReference<>(context);
            this.stickerPackIdentifier = stickerPackIdentifier;
            this.imageUri = imageUri;
            this.listener = listener;
        }

        @Override
        protected Pair<String, Uri> doInBackground(Void... voids) {
            Context context = contextWeakReference.get();
            try {
                Bitmap bitmap = Glide.with(context).asBitmap().load(imageUri).submit().get();

                File file = new File(context.getFilesDir().getAbsolutePath(), stickerPackIdentifier + "/" + TRAY_ICON_FILE_NAME);

                if (file.exists() || (!file.exists() && file.createNewFile())) {
                    FileOutputStream outputStream = new FileOutputStream(file);
                    ImageUtils.writeTrayIconImage(bitmap, outputStream);
                    outputStream.close();
                    return new Pair<>(file.getName(), Uri.fromFile(file));
                } else {
                    // There was an error creating the files
                    this.exception = new RuntimeException("There was an error creating the tray image file");
                    return null;
                }
            } catch (InterruptedException | ExecutionException | IOException ex) {
                this.exception = ex;
                return null;
            }
        }

        @Override
        protected void onPostExecute(Pair<String, Uri> stringUriPair) {
            if(stringUriPair == null) {
                listener.onError(this.exception);
            } else {
                listener.onSaved(stringUriPair.first, stringUriPair.second);
            }
        }
    }

    static class SaveStickerPackImagesTask extends AsyncTask<Void, Void, Pair<List<String>, List<Uri>>> {

        private WeakReference<Context> contextWeakReference;

        @NonNull private OnImagesSavedListener listener;
        private List<Uri> imageUris;
        private String stickerPackIdentifier;
        private Throwable exception;

        SaveStickerPackImagesTask(Context context, List<Uri> imageUris, String stickerPackIdentifier, @NonNull OnImagesSavedListener listener) {
            this.contextWeakReference = new WeakReference<>(context);
            this.imageUris = imageUris;
            this.stickerPackIdentifier = stickerPackIdentifier;
            this.listener = listener;
        }

        @Override
        protected Pair<List<String>, List<Uri>> doInBackground(Void... uris) {
            try {
                Context context = contextWeakReference.get();
                List<Uri> newUris = new ArrayList<>();
                List<String> fileNames = new ArrayList<>();
                int i = 0;
                for(Uri imageUri : imageUris) {
                    Bitmap bitmap = Glide.with(context).asBitmap().load(imageUri).submit().get();

                    File file = new File(context.getFilesDir().getAbsolutePath(), stickerPackIdentifier + "/image" + (i++) + ".webp");

                    if(file.exists() || (!file.exists() && file.createNewFile())) {
                        FileOutputStream outputStream = new FileOutputStream(file);
                        ImageUtils.writeStickerImage(bitmap, outputStream);
                        outputStream.close();
                        Log.i("NewStickerPackPresenter", "Sticker image processed: " + file.getAbsolutePath());
                        fileNames.add(file.getName());
                        newUris.add(Uri.fromFile(file));
                    } else {
                        this.exception = new RuntimeException("There was an error creating sticker image file");
                        return null;
                    }
                }

                return new Pair<>(fileNames, newUris);
            } catch (InterruptedException | ExecutionException | IOException ex) {
                this.exception = ex;
                return null;
            }
        }

        @Override
        protected void onPostExecute(Pair<List<String>, List<Uri>> pair) {
            if(pair == null) {
                listener.onError(this.exception);
            } else {
                listener.onSaved(pair.first, pair.second);
            }
        }
    }
}

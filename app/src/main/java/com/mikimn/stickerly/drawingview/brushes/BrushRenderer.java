package com.mikimn.stickerly.drawingview.brushes;


import android.graphics.Canvas;

import com.mikimn.stickerly.drawingview.DrawingEvent;

public interface BrushRenderer {
    void draw(Canvas canvas);
    void onTouch(DrawingEvent drawingEvent);
    void setBrush(Brush brush);
}

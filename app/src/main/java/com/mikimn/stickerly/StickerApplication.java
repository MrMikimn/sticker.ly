package com.mikimn.stickerly;

import android.app.Application;
import android.content.Context;

import androidx.multidex.MultiDex;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.google.android.gms.ads.MobileAds;

import io.sentry.Sentry;
import io.sentry.android.AndroidSentryClientFactory;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class StickerApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Fresco.initialize(this);
        MobileAds.initialize(getApplicationContext());
        initializeSentry();
//        initializeCalligraphy();
    }

     private void initializeSentry() {
         Context ctx = this.getApplicationContext();

         String sentryDsn = "https://6f16610016504cebbd4c2df8c0aa16b2@sentry.io/1313178";
         Sentry.init(sentryDsn, new AndroidSentryClientFactory(ctx));
     }

     private void initializeCalligraphy() {
         CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                 .setDefaultFontPath("fonts/OpenSans-Regular.ttf")
                 .setFontAttrId(R.attr.fontPath)
                 .build()
         );
     }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(base);
    }
}

/*
 * Copyright (c) WhatsApp Inc. and its affiliates.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree.
 */

package com.mikimn.stickerly.packlist;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.material.bottomappbar.BottomAppBar;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.mikimn.stickerly.BaseActivity;
import com.mikimn.stickerly.BuildConfig;
import com.mikimn.stickerly.R;
import com.mikimn.stickerly.StickerContentProvider;
import com.mikimn.stickerly.StickerPackHandler;
import com.mikimn.stickerly.StickerPackValidator;
import com.mikimn.stickerly.WhitelistCheck;
import com.mikimn.stickerly.model.StickerPack;
import com.mikimn.stickerly.newpack.NewStickerPackActivity;
import com.mikimn.stickerly.spdetails.StickerPackDetailsActivity;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class StickerPackListActivity extends BaseActivity implements View.OnClickListener, StickerPackListAdapter.OnDeleteButtonClickedListener, StickerPackListAdapter.OnEditButtonClickedListener {
    public static final String EXTRA_STICKER_PACK_LIST_DATA = "sticker_pack_list";
    private static final int STICKER_PREVIEW_DISPLAY_LIMIT = 5;
    private static final String TAG = "StickerPackList";
    private LinearLayoutManager packLayoutManager;

    private StickerPackListAdapter allStickerPacksListAdapter;
    WhiteListCheckAsyncTask whiteListCheckAsyncTask;
    ArrayList<StickerPack> stickerPackList;

    @BindView(R.id.sticker_pack_list)
    RecyclerView packRecyclerView;
    @BindView(R.id.bottom_app_bar)
    BottomAppBar bottomAppBar;
    @BindView(R.id.add_sticker_pack_fab)
    FloatingActionButton addStickerPackFAB;
    @BindView(R.id.empty_state_view)
    RelativeLayout emptyStateView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private InterstitialAd interstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sticker_pack_list);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        addStickerPackFAB.setOnClickListener(this);
        stickerPackList = getIntent().getParcelableArrayListExtra(EXTRA_STICKER_PACK_LIST_DATA);
        initBottomAppBarMenu();
        showStickerPackList(stickerPackList);
        updateEmptyState();

        interstitialAd = new InterstitialAd(this);
        interstitialAd.setAdUnitId(getString(R.string.interstitial_ad_unit_id_2));
        interstitialAd.loadAd(new AdRequest.Builder().build());

    }

    private void updateEmptyState() {
        if(stickerPackList.size() == 0) {
            emptyStateView.setVisibility(View.VISIBLE);
        } else {
            emptyStateView.setVisibility(View.GONE);
        }
    }

    private boolean validateNewStickerInfo(String packName, String publisherName) {
        if(packName.isEmpty()) {
            Toast.makeText(this, "Sticker pack name cannot be empty", Toast.LENGTH_LONG).show();
            return false;
        }

        if(packName.length() > StickerPackValidator.CHAR_COUNT_MAX) {
            Toast.makeText(this, "Sticker pack name cannot exceed " + StickerPackValidator.CHAR_COUNT_MAX + " characters", Toast.LENGTH_LONG).show();
            return false;
        }

        if(publisherName.isEmpty()) {
            Toast.makeText(this, "Publisher's name cannot be empty", Toast.LENGTH_LONG).show();
            return false;
        }

        if(publisherName.length() > StickerPackValidator.CHAR_COUNT_MAX) {
            Toast.makeText(this, "Publisher's name cannot exceed " + StickerPackValidator.CHAR_COUNT_MAX + " characters", Toast.LENGTH_LONG).show();
            return false;
        }

        return true;
    }

    public void showNewStickerPackDialog() {
        BottomSheetDialog dialog = new BottomSheetDialog(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.bottom_sheet_dialog_create_pack, null);
        dialog.setContentView(dialogView);

        final EditText packNameEditText = dialogView.findViewById(R.id.sticker_pack_name_edit_text);
        final EditText publisherNameEditText = dialogView.findViewById(R.id.sticker_pack_publisher_name_edit_text);

        final MaterialButton acceptButton = dialogView.findViewById(R.id.accept_button);
        final MaterialButton cancelButton = dialogView.findViewById(R.id.cancel_button);

        acceptButton.setOnClickListener(v -> {
            if(validateNewStickerInfo(packNameEditText.getText().toString(), publisherNameEditText.getText().toString())) {
                startActivityForResult(new Intent(StickerPackListActivity.this, NewStickerPackActivity.class)
                                .putExtra(NewStickerPackActivity.INTENT_EXTRA_PACK_NAME, packNameEditText.getText().toString())
                                .putExtra(NewStickerPackActivity.INTENT_EXTRA_PUBLISHER_NAME, publisherNameEditText.getText().toString()),
                        NewStickerPackActivity.REQUEST_CODE_NEW_PACK
                );

                dialog.dismiss();
            }
        });

        cancelButton.setOnClickListener(v -> dialog.dismiss());

        dialog.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        whiteListCheckAsyncTask = new WhiteListCheckAsyncTask(this);
        //noinspection unchecked
        whiteListCheckAsyncTask.execute(stickerPackList);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (whiteListCheckAsyncTask != null && !whiteListCheckAsyncTask.isCancelled()) {
            whiteListCheckAsyncTask.cancel(true);
        }
    }

    private void initBottomAppBarMenu() {
        // TODO Add
        // bottomAppBar.replaceMenu(R.menu.bottom_navigation);
    }

    @Override
    public void onClick(View view) {
        showNewStickerPackDialog();
    }

    private void showStickerPackList(List<StickerPack> stickerPackList) {
        allStickerPacksListAdapter = new StickerPackListAdapter(stickerPackList, onAddButtonClickedListener, this, this);
        packRecyclerView.setAdapter(allStickerPacksListAdapter);
        packLayoutManager = new LinearLayoutManager(this);
        packLayoutManager.setOrientation(RecyclerView.VERTICAL);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(
                packRecyclerView.getContext(),
                packLayoutManager.getOrientation()
        );
        packRecyclerView.addItemDecoration(dividerItemDecoration);
        packRecyclerView.setLayoutManager(packLayoutManager);
        packRecyclerView.getViewTreeObserver().addOnGlobalLayoutListener(this::recalculateColumnCount);
    }

    private StickerPackListAdapter.OnAddButtonClickedListener onAddButtonClickedListener = pack -> {
        Intent intent = new Intent();
        intent.setAction("com.whatsapp.intent.action.ENABLE_STICKER_PACK");
        intent.putExtra(StickerPackDetailsActivity.EXTRA_STICKER_PACK_ID, pack.identifier);
        intent.putExtra(StickerPackDetailsActivity.EXTRA_STICKER_PACK_AUTHORITY, BuildConfig.CONTENT_PROVIDER_AUTHORITY);
        intent.putExtra(StickerPackDetailsActivity.EXTRA_STICKER_PACK_NAME, pack.name);
        try {
            startActivityForResult(intent, StickerPackDetailsActivity.ADD_PACK);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(StickerPackListActivity.this, R.string.error_adding_sticker_pack, Toast.LENGTH_LONG).show();
        }
    };

    @Override
    public void onEditButtonClicked(StickerPack stickerPack) {
        Intent intent = new Intent(this, NewStickerPackActivity.class);
        intent.putExtra(NewStickerPackActivity.INTENT_EXTRA_IS_EDIT, true);
        intent.putExtra(NewStickerPackActivity.INTENT_EXTRA_STICKER_PACK_DATA, stickerPack);
        startActivityForResult(intent, NewStickerPackActivity.REQUEST_CODE_EDIT);
    }

    @Override
    public void onDeleteButtonClicked(StickerPack stickerPack) {
        BottomSheetDialog dialog = new BottomSheetDialog(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.bottom_sheet_dialog_delete_pack, null);
        dialog.setContentView(dialogView);

        final MaterialButton deleteButton = dialogView.findViewById(R.id.delete_button);
        final MaterialButton cancelButton = dialogView.findViewById(R.id.cancel_button);

        deleteButton.setOnClickListener(v -> {
            dialog.dismiss();
            Uri removeUri = new Uri.Builder()
                    .scheme(StickerContentProvider.CONTENT_SCHEME)
                    .authority(BuildConfig.CONTENT_PROVIDER_AUTHORITY)
                    .appendPath(StickerContentProvider.STICKERS)
                    .appendPath(stickerPack.identifier)
                    .build();

            int deleted = getContentResolver().delete(removeUri, null, null);
            if(deleted == 1) {
                allStickerPacksListAdapter.removeStickerPack(stickerPack);
                stickerPackList.remove(stickerPack);
                updateEmptyState();
            }
        });

        cancelButton.setOnClickListener(v -> dialog.dismiss());
        dialog.show();
    }

    private void recalculateColumnCount() {
        final int previewSize = getResources().getDimensionPixelSize(R.dimen.sticker_pack_list_item_preview_image_size);
        int firstVisibleItemPosition = packLayoutManager.findFirstVisibleItemPosition();
        StickerPackListItemViewHolder viewHolder = (StickerPackListItemViewHolder) packRecyclerView.findViewHolderForAdapterPosition(firstVisibleItemPosition);
        if (viewHolder != null) {
            final int max = Math.max(viewHolder.imageRowView.getMeasuredWidth() / previewSize, 1);
            int numColumns = Math.min(STICKER_PREVIEW_DISPLAY_LIMIT, max);
            allStickerPacksListAdapter.setMaxNumberOfStickersInARow(numColumns);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == StickerPackDetailsActivity.ADD_PACK) {
            if (resultCode == Activity.RESULT_CANCELED && data != null) {
                final String validationError = data.getStringExtra("validation_error");
                if (validationError != null) {
                    if (BuildConfig.DEBUG) {
                        //validation error should be shown to developer only, not users.
                        MessageDialogFragment.newInstance(R.string.title_validation_error, validationError).show(getSupportFragmentManager(), "validation error");
                    }
                    Log.e(TAG, "Validation failed:" + validationError);
                }
            } else {
                if (interstitialAd.isLoaded()) {
                    interstitialAd.show();
                } else {
                    Log.d("TAG", "The interstitial wasn't loaded yet.");
                }
            }
        } else if (requestCode == NewStickerPackActivity.REQUEST_CODE_EDIT) {
            if (resultCode == NewStickerPackActivity.RESULT_ERROR) {

                MessageDialogFragment
                        .newInstance(R.string.title_pack_edit_error, "An error occurred trying to edit the sticker pack")
                        .show(getSupportFragmentManager(), "edit error");
            } else if(resultCode == RESULT_OK) {

                (new LoadListAsyncTask(StickerPackListActivity.this)).execute();
            }
        } else if (requestCode == NewStickerPackActivity.REQUEST_CODE_NEW_PACK) {
            if (resultCode == NewStickerPackActivity.RESULT_ERROR) {

                MessageDialogFragment
                        .newInstance(R.string.title_pack_edit_error, "An error occurred trying to create the sticker pack")
                        .show(getSupportFragmentManager(), "edit error");
            } else if(resultCode == RESULT_OK) {

                (new LoadListAsyncTask(StickerPackListActivity.this)).execute();
            }
        }
    }


    static class WhiteListCheckAsyncTask extends AsyncTask<List<StickerPack>, Void, List<StickerPack>> {
        private final WeakReference<StickerPackListActivity> stickerPackListActivityWeakReference;

        WhiteListCheckAsyncTask(StickerPackListActivity stickerPackListActivity) {
            this.stickerPackListActivityWeakReference = new WeakReference<>(stickerPackListActivity);
        }

        @SafeVarargs
        @Override
        protected final List<StickerPack> doInBackground(List<StickerPack>... lists) {
            List<StickerPack> stickerPackList = lists[0];
            final StickerPackListActivity stickerPackListActivity = stickerPackListActivityWeakReference.get();
            if (stickerPackListActivity == null) {
                return stickerPackList;
            }
            for (StickerPack stickerPack : stickerPackList) {
                stickerPack.setIsWhitelisted(WhitelistCheck.isWhitelisted(stickerPackListActivity, stickerPack.identifier));
            }
            return stickerPackList;
        }

        @Override
        protected void onPostExecute(List<StickerPack> stickerPackList) {
            final StickerPackListActivity stickerPackListActivity = stickerPackListActivityWeakReference.get();
            if (stickerPackListActivity != null) {
                stickerPackListActivity.allStickerPacksListAdapter.notifyDataSetChanged();
            }
        }
    }

    void showStickerPacks(List<StickerPack> packs) {
        allStickerPacksListAdapter.setStickerPacks(packs);
        this.stickerPackList = new ArrayList<>(packs); // TODO Check that
        updateEmptyState();
    }

    static class LoadListAsyncTask extends AsyncTask<Void, Void, Pair<String, ArrayList<StickerPack>>> {
        private final WeakReference<StickerPackListActivity> contextWeakReference;

        LoadListAsyncTask(StickerPackListActivity activity) {
            this.contextWeakReference = new WeakReference<>(activity);
        }

        @Override
        protected Pair<String, ArrayList<StickerPack>> doInBackground(Void... voids) {
            ArrayList<StickerPack> stickerPackList;
            try {
                final Context context = contextWeakReference.get();
                if (context != null) {
                    stickerPackList = StickerPackHandler.fetchStickerPacks(context);
                    for (StickerPack stickerPack : stickerPackList) {
                        StickerPackValidator.verifyStickerPackValidity(context, stickerPack);
                    }
                    return new Pair<>(null, stickerPackList);
                } else {
                    return new Pair<>("could not fetch sticker packs", null);
                }
            } catch (Exception e) {
                Log.e("StickerPackListActivity", "error fetching sticker packs", e);
                return new Pair<>(e.getMessage(), null);
            }
        }

        @Override
        protected void onPostExecute(Pair<String, ArrayList<StickerPack>> stringListPair) {

            final StickerPackListActivity entryActivity = contextWeakReference.get();
            if (entryActivity != null) {
                if (stringListPair.first != null) {
                    MessageDialogFragment
                            .newInstance(R.string.title_pack_edit_error, "An error occured trying to edit the sticker pack")
                            .show(entryActivity.getSupportFragmentManager(), "edit error");
                } else {
                    entryActivity.showStickerPacks(stringListPair.second);
                }
            }
        }
    }
}

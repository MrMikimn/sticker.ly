/*
 * Copyright (c) WhatsApp Inc. and its affiliates.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree.
 */

package com.mikimn.stickerly.packlist;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mikimn.stickerly.R;

import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

class StickerPackListItemViewHolder extends RecyclerView.ViewHolder {

    View container;
    @BindView(R.id.sticker_pack_title)                  TextView titleView;
    @BindView(R.id.sticker_pack_publisher)              TextView publisherView;
    @BindView(R.id.sticker_pack_filesize)               TextView filesizeView;
    @BindView(R.id.add_button_on_list)                  ImageView addButton;
    @BindView(R.id.edit_button_on_list)                 ImageView editButton;
    @BindView(R.id.delete_button_on_list)               ImageView deleteButton;
    @BindView(R.id.sticker_packs_list_item_image_list)  LinearLayout imageRowView;

    StickerPackListItemViewHolder(final View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.container = itemView;
    }
}
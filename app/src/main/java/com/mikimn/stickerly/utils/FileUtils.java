package com.mikimn.stickerly.utils;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class FileUtils {

    /**
     * Copy an asset file or directory to internal storage
     * @param context A valid context
     * @param path The relative path of the file/directory inside the assets/ folder
     */
    public static void copyAssetFileOrDirectory(Context context, String path) {
        AssetManager assetManager = context.getAssets();
        String assets[];
        try {
            assets = assetManager.list(path);
            if (assets.length > 0) {
                File dir = new File(context.getFilesDir().getAbsolutePath(), path);
                if(!dir.exists()) {
                    dir.mkdir();
                }

                for (String asset : assets) {
                    copyAssetFileOrDirectory(context, path.equals("") ? asset : path + "/" + asset);
                }
            } else if(assets.length == 0) {
                File dir = new File(context.getFilesDir().getAbsolutePath(), path);
                if(!dir.exists()) {
                    dir.createNewFile();
                }
                InputStream in = null;
                OutputStream out = null;
                try {
                    in = assetManager.open(path);
                    out = new FileOutputStream(dir);
                    copyFile(in, out);
                } catch(IOException e) {
                    Log.e("tag", "Failed to copy asset file: " + path, e);
                }
            }
        } catch (IOException ex) {
            Log.e("tag", "I/O Exception", ex);
        }
    }

    private static void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while((read = in.read(buffer)) != -1){
            out.write(buffer, 0, read);
        }
    }

    public static String md5(String s) {
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuffer hexString = new StringBuffer();
            for (int i=0; i<messageDigest.length; i++)
                hexString.append(Integer.toHexString(0xFF & messageDigest[i]));
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public interface PermissionListener {
        void onPermissionsInteracted(boolean granted, Throwable t);
    }

    public static void requestReadWriteAccess(@NonNull AppCompatActivity activity, @Nullable PermissionListener listener) {
        Dexter.withActivity(activity)
                .withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if(listener != null) {
                            if(report.areAllPermissionsGranted()) {
                                listener.onPermissionsInteracted(true, null);
                            } else {
                                // TODO Better handler
                                List<PermissionDeniedResponse> deniedResponses = report.getDeniedPermissionResponses();
                                List<PermissionGrantedResponse> grantedResponses = report.getGrantedPermissionResponses();

                                StringBuilder deniedBuilder = new StringBuilder();
                                StringBuilder grantedBuilder = new StringBuilder();
                                for(PermissionDeniedResponse r : deniedResponses) deniedBuilder.append(r.getPermissionName()).append(",");
                                for(PermissionGrantedResponse r : grantedResponses) grantedBuilder.append(r.getPermissionName()).append(",");

                                IllegalStateException ex =  new IllegalStateException(
                                        "Not all permissions granted by the user." +
                                                "Granted permissions: " + grantedBuilder.toString() +
                                                "Denied permissions:" + deniedBuilder.deleteCharAt(grantedBuilder.length() - 1).toString());
                                listener.onPermissionsInteracted(false, ex);
                            }
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        // TODO Handle
                        token.cancelPermissionRequest();
                        if(listener != null) {
                            listener.onPermissionsInteracted(false, new RuntimeException("Permissions Denied"));
                        }
                    }
                }).check();
    }

    public static void addImageToGalleryProvider(@NonNull Context context, String imagePath) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(imagePath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        context.sendBroadcast(mediaScanIntent);
    }

    public static File createCameraImageFile(@NonNull Context context) throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        return File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
    }
}

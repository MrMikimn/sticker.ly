package com.mikimn.stickerly.utils;

import android.graphics.Bitmap;

import java.io.OutputStream;

public class ImageUtils {

    private static final int STICKER_IMAGE_SIZE = 512;
    private static final int TRAY_ICON_SIZE = 96;
    private static final int STICKER_MARGIN = 16;
    private static final int STICKER_IMAGE_QUALITY = 50;
    private static final int TRAY_ICON_QUALITY = 50;

    public static Bitmap processStickerImage(Bitmap source) {
        BitmapProcessor bitmapProcessor = BitmapProcessor.with(source)
                .trim()
                .scale(STICKER_IMAGE_SIZE - 2 * STICKER_MARGIN, STICKER_IMAGE_SIZE - 2 * STICKER_MARGIN, true)
                .margin(STICKER_MARGIN);
        return bitmapProcessor.process();
    }

    public static Bitmap processTrayImage(Bitmap source) {
        BitmapProcessor bitmapProcessor = BitmapProcessor.with(source)
                .trim()
                .scale(TRAY_ICON_SIZE, TRAY_ICON_SIZE, true);
        return bitmapProcessor.process();
    }

    public static void writeStickerImage(Bitmap source, OutputStream stream) {
        processStickerImage(source).compress(Bitmap.CompressFormat.WEBP, STICKER_IMAGE_QUALITY, stream);
    }

    public static void writeTrayIconImage(Bitmap source, OutputStream stream) {
        processTrayImage(source).compress(Bitmap.CompressFormat.WEBP, TRAY_ICON_QUALITY, stream);
    }
}

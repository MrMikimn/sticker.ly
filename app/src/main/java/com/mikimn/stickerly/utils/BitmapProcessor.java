package com.mikimn.stickerly.utils;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.util.Log;

public class BitmapProcessor {

    private Bitmap mBitmap;

    private BitmapProcessor(Bitmap source) {
        this.mBitmap = source;
    }

    private static Bitmap bitmapTrim(Bitmap sourceBitmap) {
        if(sourceBitmap == null) return null;

        int minX = sourceBitmap.getWidth();
        int minY = sourceBitmap.getHeight();
        int maxX = -1;
        int maxY = -1;
        for(int y = 0; y < sourceBitmap.getHeight(); y++) {
            for(int x = 0; x < sourceBitmap.getWidth(); x++) {
                int alpha = (sourceBitmap.getPixel(x, y) >> 24) & 255;
                if(alpha > 0) {
                    if(x < minX) minX = x;
                    if(x > maxX) maxX = x;
                    if(y < minY) minY = y;
                    if(y > maxY) maxY = y;
                }
            }
        }
        if((maxX < minX) || (maxY < minY)) return null; // Bitmap is entirely transparent

        // crop bitmap to non-transparent area and return:
        return Bitmap.createBitmap(sourceBitmap, minX, minY, (maxX - minX) + 1, (maxY - minY) + 1);
    }

    private static Bitmap bitmapScaleAspectRatio(Bitmap source, int width, int height) {
        Bitmap background = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

        float originalWidth = source.getWidth();
        float originalHeight = source.getHeight();

        Canvas canvas = new Canvas(background);

        float scale = Math.min(width / originalWidth, height / originalHeight);

        float xTranslation = (width - originalWidth * scale) / 2.0f;
        float yTranslation = (height - originalHeight * scale) / 2.0f;

        Matrix transformation = new Matrix();
        transformation.postTranslate(xTranslation, yTranslation);
        transformation.preScale(scale, scale);

        canvas.drawBitmap(source, transformation, null);

        return background;
    }

    private static Bitmap bitmapApplyMargin(Bitmap source, int margin) {
        Bitmap background = Bitmap.createBitmap(source.getWidth() + 2 * margin, source.getHeight() + 2 * margin, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(background);
        canvas.drawBitmap(source, margin, margin, null);
        Log.i("Processor", "Creating image with h: " + canvas.getHeight() + ", w: " + canvas.getWidth());
        return background;
    }

    public static BitmapProcessor with(Bitmap bitmap) {
        return new BitmapProcessor(bitmap);
    }

    public BitmapProcessor trim() {
        if(mBitmap == null) return this;

        mBitmap = bitmapTrim(mBitmap);
        return this;
    }

    public BitmapProcessor resize(int width, int height, boolean filter) {
        if(mBitmap == null) return this;

        mBitmap = Bitmap.createScaledBitmap(mBitmap, width, height, filter);
        return this;
    }

    public BitmapProcessor scale(int width, int height, boolean maintainAspectRatio) {
        if(mBitmap == null) return this;

        if(maintainAspectRatio) {
            mBitmap = bitmapScaleAspectRatio(mBitmap, width, height);
            return this;
        } else {
            return resize(width, height, false);
        }
    }

    public BitmapProcessor margin(int margin) {
        if(mBitmap == null) return this;

        mBitmap = bitmapApplyMargin(mBitmap, margin);
        return this;
    }

    public Bitmap process() {
        // TODO Handle null?
        return mBitmap;
    }

}

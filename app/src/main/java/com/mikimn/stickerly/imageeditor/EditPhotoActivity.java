package com.mikimn.stickerly.imageeditor;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ProgressBar;
import android.widget.SeekBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;

import com.bumptech.glide.Glide;
import com.divyanshu.colorseekbar.ColorSeekBar;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.mikimn.stickerly.BaseActivity;
import com.mikimn.stickerly.R;
import com.mikimn.stickerly.drawingview.DrawingView;
import com.mikimn.stickerly.drawingview.brushes.Brushes;
import com.mikimn.stickerly.utils.BitmapProcessor;
import com.mikimn.stickerly.utils.FileUtils;
import com.mikimn.stickerly.utils.ImageUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EditPhotoActivity extends BaseActivity implements BottomNavigationView.OnNavigationItemSelectedListener,
        SeekBar.OnSeekBarChangeListener, ColorSeekBar.OnColorChangeListener {

    public static final int REQUEST_EDIT_PHOTO = 200;

    public static final String INTENT_EXTRA_IMAGE_URI = "extraImageUri";
    public static final String INTENT_EXTRA_IMAGE_NAME = "extraImageName";
    // public static final String INTENT_EXTRA_IMAGE = "extraImageData";

    @BindView(R.id.drawing_view)
    DrawingView drawingView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.edit_photo_bottom_navigation)
    BottomNavigationView bottomNavigationView;
    @BindView(R.id.brush_size_seekbar)
    SeekBar brushSizeSeekbar;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    @BindView(R.id.color_seek_bar)
    ColorSeekBar colorSeekBar;

    @Nullable
    private String imagePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_photo);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getWindow().setStatusBarColor(Color.BLACK);

        imagePath = getIntent().getStringExtra(INTENT_EXTRA_IMAGE_NAME);
        LoadImageTask task = new LoadImageTask(this, imagePath);
        drawingView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                drawingView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                task.execute();
            }
        });
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.photo_editor_action_eraser:
                drawingView.getBrushSettings().setSelectedBrush(Brushes.ERASER);
                colorSeekBar.setEnabled(false);
                colorSeekBar.setFocusable(false);
                return true;
            case R.id.photo_editor_action_brush:
                drawingView.getBrushSettings().setColor(colorSeekBar.getColor());
                drawingView.getBrushSettings().setSelectedBrush(Brushes.PEN);
                colorSeekBar.setEnabled(true);
                colorSeekBar.setFocusable(true);
                return true;
            case R.id.photo_editor_action_airbrush:
                drawingView.getBrushSettings().setColor(colorSeekBar.getColor());
                drawingView.getBrushSettings().setSelectedBrush(Brushes.AIR_BRUSH);
                colorSeekBar.setEnabled(true);
                colorSeekBar.setFocusable(true);
                return true;
            case R.id.photo_editor_action_pencil:
                drawingView.getBrushSettings().setColor(colorSeekBar.getColor());
                drawingView.getBrushSettings().setSelectedBrush(Brushes.CALLIGRAPHY);
                colorSeekBar.setEnabled(true);
                colorSeekBar.setFocusable(true);
                return true;
        }
        return false;
    }

    private File saveBitmap(String imagePath) {
        Bitmap processed = ImageUtils.processStickerImage(drawingView.exportDrawing());
        if(processed == null) {
            Snackbar.make(drawingView, "Nothing was drawn! Empty stickers aren't allowed", Snackbar.LENGTH_LONG).show();
            return null;
        } else {

            File imageFile = new File(getCacheDir().getAbsolutePath(), FileUtils.md5(imagePath == null ? (new Date()).toString() : imagePath) + ".webp");
            try (FileOutputStream out = new FileOutputStream(imageFile)) {
                processed.compress(Bitmap.CompressFormat.WEBP, 50, out);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return imageFile;
        }
    }

    private void saveImageFile(String filePath) {
        FileUtils.requestReadWriteAccess(this, ((granted, t) -> {
            if(granted) {
                try {
                    saveBitmap(filePath);
                } catch (SecurityException ex) {
                    Log.e("PhotoEditor","Failed to save Image", ex);
                    Snackbar.make(drawingView, "There was an error saving the edited image. Please try again", Snackbar.LENGTH_LONG).show();
                }
            } else {
                Snackbar.make(drawingView, "There was an error with permissions. Make sure you grant the app Read/Write access", Snackbar.LENGTH_LONG).show();
                t.printStackTrace(); // TODO Handle
            }
        }));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.photo_editor_top_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                setResult(RESULT_CANCELED);
                finish();
                return true;
            }
            case R.id.edit_photo_save:
                progressBar.setVisibility(View.VISIBLE);
                (new SaveImageTask(this, imagePath)).execute();
                return true;
            case R.id.edit_photo_undo:
                drawingView.undo();
                return true;
            case R.id.edit_photo_redo:
                drawingView.redo();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        switch (seekBar.getId()) {
            case R.id.brush_size_seekbar:
                float scale = progress / (float)(seekBar.getMax());
                drawingView.getBrushSettings().setSelectedBrushSize(scale);
                break;
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {}

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {}

    @Override
    public void onColorChangeListener(int color) {
        drawingView.getBrushSettings().setColor(color);
    }

    private void showLoadingBar() {
        this.progressBar.setVisibility(View.VISIBLE);
    }

    private void hideLoadingBar() {
        this.progressBar.setVisibility(View.GONE);
    }

    private static class SaveImageTask extends AsyncTask<Void, Void, File> {
        private String imagePath;
        private WeakReference<EditPhotoActivity> weakReference;

        SaveImageTask(EditPhotoActivity activity, String imagePath) {
            this.weakReference = new WeakReference<>(activity);
            this.imagePath = imagePath;
        }

        @Override
        protected File doInBackground(Void... voids) {
            return weakReference.get().saveBitmap(imagePath);
        }

        @Override
        protected void onPostExecute(File imageFile) {
            EditPhotoActivity activity = weakReference.get();
            activity.progressBar.setVisibility(View.GONE);
            if(imageFile != null) {
                Intent data = new Intent();
                data.putExtra(INTENT_EXTRA_IMAGE_URI, Uri.fromFile(imageFile));
                data.putExtra(INTENT_EXTRA_IMAGE_NAME, imageFile.getName());
                activity.setResult(RESULT_OK, data);
                activity.finish();
            }
        }
    }

    private static class LoadImageTask extends AsyncTask<Void, Void, Bitmap> {

        private WeakReference<EditPhotoActivity> weakReference;
        private String imagePath;

        LoadImageTask(EditPhotoActivity activity, String imagePath) {
            this.weakReference = new WeakReference<>(activity);
            this.imagePath = imagePath;
        }

        @Override
        protected Bitmap doInBackground(Void... voids) {
            weakReference.get().runOnUiThread(() -> weakReference.get().showLoadingBar());
            try {
                return Glide.with(weakReference.get()).asBitmap().load(imagePath).submit().get();
            } catch (Exception ex) {
                ex.printStackTrace(); // TODO Handle
                return Bitmap.createBitmap(512, 512, Bitmap.Config.ARGB_8888);
            }
        }

        @Override
        protected void onPostExecute(Bitmap imageBitmap) {
            EditPhotoActivity activity = weakReference.get();
            final DrawingView drawingView = activity.drawingView;
            final BottomNavigationView bottomNavigationView = activity.bottomNavigationView;
            final SeekBar brushSizeSeekbar = activity.brushSizeSeekbar;
            final ColorSeekBar colorSeekBar = activity.colorSeekBar;

            if(imageBitmap == null) {
                imageBitmap = Bitmap.createBitmap(512, 512, Bitmap.Config.ARGB_8888);
            }

            drawingView.setDrawingBackground(Color.TRANSPARENT);
            drawingView.getBrushSettings().setSelectedBrush(Brushes.ERASER);
            drawingView.setUndoAndRedoEnable(true);

            drawingView.setBackgroundImage(BitmapProcessor.with(imageBitmap).scale(drawingView.getWidth(), drawingView.getHeight(), true).process());
            activity.hideLoadingBar();

            bottomNavigationView.setOnNavigationItemSelectedListener(activity);

            brushSizeSeekbar.setOnSeekBarChangeListener(activity);
            brushSizeSeekbar.setMax(100);
            brushSizeSeekbar.setProgress(50);

            colorSeekBar.setOnColorChangeListener(activity);
        }
    }
}
